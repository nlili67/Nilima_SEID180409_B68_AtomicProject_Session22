<?php

    require_once ("../../../vendor/autoload.php");

    $obj  = new \App\Summary\Summary();

    use App\Utility\Utility;

    $obj->setData($_POST);

    $obj->store();
    Utility::redirect("index.php");