<?php

require_once ("../../../vendor/autoload.php");

use App\Message\Message;
use App\Utility\Utility;

use App\Summary\Summary;

$obj = new Summary();
$obj->setData($_GET);

$oneData = $obj->delete();

Utility::redirect("index.php");