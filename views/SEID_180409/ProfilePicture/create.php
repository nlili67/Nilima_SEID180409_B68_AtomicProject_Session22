<?php

require_once ("../../../vendor/autoload.php");

use App\Message\Message;


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hobbie</title>
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<style>
    #color{
    font-family:"Imprint MT Shadow";
    font-size: 20px;
    }
</style>
</head>
<body background="../imagge/2.jpg">

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">aotomic Project</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"id="color"><a href="../index/index.php">Home</a></li>
            <li class="active"id="color"><a href="../BirthDate/index.php">Birth Date</a></li>
            <li class="active"id="color"><a href="../BookTitle/index.php">Book Title</a></li>
            <li class="active"id="color"><a href="#">City</a></li>
            <li class="active"id="color"><a href="../Email/index.php">Email</a></li>
            <li class="active"id="color"><a href="../Gender/index.php">Gender</a></li>
            <li class="active"id="color"><a href="../Hobbies/index.php">Hobbies</a></li>
            <li class="active"id="color"><a href="../ProfilePicture/index.php">Profile Picture</a></li>
            <li class="active"id="color"><a href="../summary/index.php">Summary</a></li>


        </ul>
    </div>
</nav>




<div id="MessageShowDiv" style="height: 20px">
    <div id="message" class="btn-danger text-center" >
        <?php
        if(isset($_SESSION['message'])){
            echo Message::message();
        }
        ?>
    </div>
</div>

<div class="col-lg-4"></div>

<div class="col-lg-8">
    <h1 style="color: red"> Profile Picture - Add Form </h1>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <form action="store.php" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="Name"style="color: red">Name</label>
                        <input type="text" class="form-control" placeholder="Name" name="Name" required="">
                    </div>

                    <div class="form-group">
                        <label for="ProfilePicture" style="color: red">Student Image</label>
                        <input type="file" class="form-control"" name="File2Upload" required="">
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>



    </div>




</div>
<div class="col-lg-4">

</div>




<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
<script>


    $(function ($) {

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);
        $("#message").fadeOut(500);

    });


</script>


</body>
</html>
