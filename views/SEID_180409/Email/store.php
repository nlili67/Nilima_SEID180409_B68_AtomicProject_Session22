<?php

    require_once ("../../../vendor/autoload.php");

    $obj  = new \App\Email\Email();

    use App\Utility\Utility;

    $obj->setData($_POST);

    $obj->store();

    Utility::redirect("index.php");
