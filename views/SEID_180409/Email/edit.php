<?php

require_once ("../../../vendor/autoload.php");

use App\Message\Message;

use App\Email\Email;

$obj = new Email();
$obj->setData($_GET);

$oneData = $obj->view();



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hobbie</title>
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
<style>
    #color{
    font-family:"Imprint MT Shadow";
    font-size: 20px;
    }
</style>
</head>
<body background="../imagge/1.jpg">

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Atomic Project</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"id="color"><a href="../index/index.php">Home</a></li>
            <li class="active"id="color"><a href="../BirthDate/index.php">Birth Date</a></li>
            <li class="active"id="color"><a href="../BookTitle/index.php">Book Title</a></li>
            <li class="active"id="color"><a href="../City/index.php">City</a></li>
            <li class="active"id="color"><a href="#">Email</a></li>
            <li class="active"id="color"><a href="../Gender/index.php">Gender</a></li>
            <li class="active"id="color"><a href="../Hobbies/index.php">Hobbies</a></li>
            <li class="active"id="color"><a href="../ProfilePicture/index.php">Profile Picture</a></li>
            <li class="active"id="color"><a href="../summary/index.php">Summary</a></li>


        </ul>
    </div>
</nav>



<h1> Profile Picture - Edit Form </h1>

<div id="MessageShowDiv" style="height: 20px">
    <div id="message" class="btn-danger text-center" >
        <?php
        if(isset($_SESSION['message'])){
            echo Message::message();
        }
        ?>
    </div>
</div>


    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <form action="update.php" method="POST" enctype="multipart/form-data">

                    <div class="form-group">
                        <input type="hidden" class="form-control" value="<?php echo $oneData->id ?>" name="id" >
                    </div>



                    <div class="form-group">
                        <label for="Name">Customer_ID</label>
                        <input type="text" class="form-control" value="<?php echo $oneData->customer_Id ?>" name="cusId" required="">
                    </div>

                    <div class="form-group">
                        <label for="ProfilePicture">Customer_Name</label>
                        <input type="text" class="form-control" value="<?php echo $oneData->customer_name ?>" name="cusName" required="" >
                    </div>


                    <div class="form-group">
                        <label for="Email">Email</label>
                        <input type="text" class="form-control" value="<?php echo $oneData->email ?>" name="email" required="" >
                    </div>



                    <button type="submit" class="btn btn-primary">Update</button>

                </form>
            </div>
        </div>

      

    </div>



<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
<script>


    $(function ($) {

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);
        $("#message").fadeOut(500);

    });


</script>


</body>
</html>
