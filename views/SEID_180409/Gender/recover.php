<?php

require_once ("../../../vendor/autoload.php");

use App\Message\Message;
use App\Utility\Utility;

use App\Gender\Gender;

$obj = new Gender();
$obj->setData($_GET);

$oneData = $obj->recover();

Utility::redirect("index.php");