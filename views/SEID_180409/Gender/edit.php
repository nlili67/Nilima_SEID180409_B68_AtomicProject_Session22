<?php
   require_once ("../../../vendor/autoload.php");
   if(!isset($_SESSION)) session_start();
   use App\Message\Message;
   use App\Gender\Gender;

   $obj = new Gender();
   $obj->setData($_GET);
   $oneData = $obj->view();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    
    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    
</head>
<body background="../imagge/1.jpg">


<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">aotomic Project</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"id="color"><a href="../index/index.php">Home</a></li>
            <li class="active"id="color"><a href="../BirthDate/index.php">Birth Date</a></li>
            <li class="active"id="color"><a href="../BookTitle/index.php">Book Title</a></li>
            <li class="active"id="color"><a href="#">City</a></li>
            <li class="active"id="color"><a href="../Email/index.php">Email</a></li>
            <li class="active"id="color"><a href="../Gender/index.php">Gender</a></li>
            <li class="active"id="color"><a href="../Hobbies/index.php">Hobbies</a></li>
            <li class="active"id="color"><a href="../ProfilePicture/index.php">Profile Picture</a></li>
            <li class="active"id="color"><a href="../summary/index.php">Summary</a></li>


        </ul>
    </div>
</nav>

<div id="message" class="bg-primary text-center" > <?php echo Message::message() ?> </div>

<div class="container bg-primary" style="margin-top: 100px">

    <h1 style="text-align: center"> Gender_Edit Form </h1>

    <div class="col-md-2"> </div>


    <div class="col-md-8" style="margin-top: 50px; margin-bottom: 50px">


        <form action="update.php" method="post">



            <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $oneData->id ?>"
            </div>

            <div class="form-group">
                <label for="Name">Applicant's Name</label>
                <input type="text" class="form-control" placeholder="Name" name="applicantName" value="<?php echo $oneData->Applicant_name ?>">
            </div>

            <div class="form-group"><br>
                <label class=" ">Gender</label><br>
                <div class="col-sm-2">
                    <div class="row">
                        <div class="col-sm-5">
                            <label class="checkbox-inline">
                                <input type="radio" name="Gender" value="Male"value="<?php echo $oneData->gender?>">Male
                            </label>
                        </div>

                        <div class="col-sm-5">
                            <label class="checkbox-inline">
                                <input type="radio" name="Gender" value="Female"value="<?php echo $oneData->gender?>">Female
                            </label>
                        </div>

                        <div class="col-sm-4">

                        </div>

                    </div>
                    </div>
                    </div><br><br>



            <button type="submit" class="btn btn-default">Update</button>




        </form>

    </div>


    <div class="col-md-2" > </div>


</div>

<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

<script>


    $(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    });




</script>


</body>
</html>