<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Gender\Gender;

$obj = new Gender();
$obj->setData($_GET);

$oneData  =  $obj->view();


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>



    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <style>

        #color{
            font-family:"Imprint MT Shadow";
            font-size: 20px;
        }
    </style>



</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Atomic Project</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"id="color"><a href="../index/index.php">Home</a></li>
            <li class="active"id="color"><a href="../BirthDate/index.php">Birth Date</a></li>
            <li class="active"id="color"><a href="../BookTitle/index.php">Book Title</a></li>
            <li class="active"id="color"><a href="../City/index.php">City</a></li>
            <li class="active"id="color"><a href="../Email/index.php">Email</a></li>
            <li class="active"id="color"><a href="../Gender/index.php">Gender</a></li>
            <li class="active"id="color"><a href="../Hobbies/index.php">Hobbies</a></li>
            <li class="active"id="color"><a href="#">Profile Picture</a></li>
            <li class="active"id="color"><a href="../summary/index.php">Summary</a></li>


        </ul>
    </div>
</nav>


<div class="container">


<?php

         echo "
             <h1> Single Profile Information </h1>
               
             <table class='table table-bordered table-striped'>
             
                    <tr>                   
                        <td>  <b>ID</b>  </td>                
                        <td>  <b>$oneData->id</b>  </td>                
                      
                    </tr>
        
                     <tr>                   
                        <td>  <b>Applicant_Name</b>  </td>                
                        <td>  <b>$oneData->Applicant_name</b>  </td>                
                      
                    </tr>
                         
                     <tr>                   
                        <td>  <b>Gender</b>  </td>                
                        <td><b>$oneData->gender</b> </td>
                      
                    </tr>
                    
                    <tr>                  
                        <td class='text-center' colspan='2'>  <a class='btn bg-primary' href='index.php'> Back to Active List</a> </td>
                    </tr>
                
             
             </table>
             
             
             

         ";


?>

</div>

</body>
</html>