<?php

require_once ("../../../vendor/autoload.php");

use App\Message\Message;


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hobbie</title>
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <style>
        #color{
            font-family:"Imprint MT Shadow";
            font-size: 20px;
        }


    </style>
</head>
<body background="../imagge/2.jpg">

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">aotomic Project</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"id="color"><a href="../index/index.php">Home</a></li>
            <li class="active"id="color"><a href="../BirthDate/index.php">Birth Date</a></li>
            <li class="active"id="color"><a href="../BookTitle/index.php">Book Title</a></li>
            <li class="active"id="color"><a href="#">City</a></li>
            <li class="active"id="color"><a href="../Email/index.php">Email</a></li>
            <li class="active"id="color"><a href="../Gender/index.php">Gender</a></li>
            <li class="active"id="color"><a href="../Hobbies/index.php">Hobbies</a></li>
            <li class="active"id="color"><a href="../ProfilePicture/index.php">Profile Picture</a></li>
            <li class="active"id="color"><a href="../summary/index.php">Summary</a></li>


        </ul>
    </div>
</nav>


<div id="MessageShowDiv" style="height: 20px">
    <div id="message" class="btn-danger text-center" >
        <?php
        if(isset($_SESSION['message'])){
            echo Message::message();
        }
        ?>
    </div>
</div>

<div id="MessageShowDiv" style="height: 20px">
    <div id="message" class="btn-danger text-center" >
        <?php
        if(isset($_SESSION['message'])){
            echo Message::message();
        }
        ?>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-5">
            <form action="store.php" method="POST">
                <div class="form-group">
                    <label for="Name" style="color: red">Applicant's Name</label>
                    <input type="text" class="form-control" placeholder="Name" name="applicantName">
                </div>

                <div class="form-group"><br>
                    <label class=" " style="color: red">Gender</label><br>
                    <div class="col-sm-2">
                        <div class="row">
                            <div class="col-sm-5">
                                <label class="checkbox-inline"style="color: red">
                                    <input type="radio" name="Gender" value="Male">Male
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <label class="checkbox-inline"style="color: red">
                                    <input type="radio" name="Gender" value="Female">Female
                                </label>
                            </div>
                            <div class="col-sm-4">

                            </div>
                        </div>
                    </div>
                </div><br><br>

                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="submit" class="btn btn-primary">Add&Save</button>
                <button type="reset" class="btn btn-primary">Reset</button>

            </form>
        </div>
    </div>
</div>

<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
<script>


    $(function ($) {

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);
        $("#message").fadeOut(500);

    });


</script>


</body>
</html>
