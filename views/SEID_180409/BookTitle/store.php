<?php
    require_once ("../../../vendor/autoload.php");

    $obj  = new \App\BookTitle\BookTitle();

    use App\Utility\Utility;

    $obj->setData($_POST);

    $obj->store();

    Utility::redirect("index.php");
