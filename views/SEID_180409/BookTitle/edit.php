<?php
   require_once ("../../../vendor/autoload.php");
   if(!isset($_SESSION)) session_start();
   use App\Message\Message;
   use App\BookTitle\BookTitle;

   $obj = new BookTitle();
   $obj->setData($_GET);
   $oneData = $obj->view();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    
    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    
    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    
</head>
<body>

<div id="message" class="bg-primary text-center" > <?php echo Message::message() ?> </div>

<div class="container bg-primary" style="margin-top: 100px">

    <h1 style="text-align: center"> Book Title - Edit Form </h1>

    <div class="col-md-2"> </div>


    <div class="col-md-8" style="margin-top: 50px; margin-bottom: 50px">


        <form action="update.php" method="post">
         

            
            
            <div class="form-group">
                <label for="BookTitle">Book Title</label>
                <input type="text" class="form-control" name="BookTitle" value="<?php echo $oneData->book_title ?>">
            </div>



            <div class="form-group">
                <label for="AuthorName">Author Name</label>
                <input type="text" class="form-control" name="authorName" value="<?php echo $oneData->author_name ?>">
            </div>


            <div class="form-group">
                <input type="hidden" name="id" value="<?php echo $oneData->id ?>"
            </div>


            <button type="submit" class="btn btn-default">Update</button>




        </form>

    </div>


    <div class="col-md-2" > </div>


</div>

<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

<script>


    $(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    });




</script>


</body>
</html>