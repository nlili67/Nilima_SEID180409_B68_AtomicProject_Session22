<?php

require_once ("../../../vendor/autoload.php");

use App\Message\Message;
use App\Utility\Utility;

use App\City\City;

$obj = new City();
$obj->setData($_GET);

$oneData = $obj->delete();

Utility::redirect("index.php");