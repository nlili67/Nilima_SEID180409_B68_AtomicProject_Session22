<?php

    require_once ("../../../vendor/autoload.php");

    $obj  = new \App\City\City();

    use App\Utility\Utility;

    $obj->setData($_POST);

    $obj->store();

    Utility::redirect("index.php");
