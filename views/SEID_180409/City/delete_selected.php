<?php

require_once ("../../../vendor/autoload.php");
use App\City\City;
use App\Utility\Utility;

$obj = new City();

$selectedIDs =   $_POST["mark"];

$obj->deleteMultiple($selectedIDs);

$path = $_SERVER['HTTP_REFERER'];

Utility::redirect($path);

