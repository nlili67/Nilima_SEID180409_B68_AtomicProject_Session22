<?php

require_once ("../../../vendor/autoload.php");
use App\BirthDate\BirthDate;
use App\Utility\Utility;

$obj = new BirthDate();

$selectedIDs = $_POST["mark"];

$obj->recoverMultiple($selectedIDs);

Utility::redirect("index.php");

