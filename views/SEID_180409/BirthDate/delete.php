<?php

require_once ("../../../vendor/autoload.php");

use App\Message\Message;
use App\Utility\Utility;

use App\Birthdate\BirthDate;

$obj = new BirthDate();
$obj->setData($_GET);

$oneData = $obj->delete();

Utility::redirect("index.php");