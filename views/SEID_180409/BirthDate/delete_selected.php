<?php
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\BirthDate\BirthDate;

$selectedIDs = $_POST['mark'];

$obj = new BirthDate();

$obj->deleteMultiple($selectedIDs);


Utility::redirect("index.php");