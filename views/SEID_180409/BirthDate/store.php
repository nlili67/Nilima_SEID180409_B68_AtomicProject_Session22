<?php

    require_once ("../../../vendor/autoload.php");

    $obj  = new \App\BirthDate\BirthDate();
     use App\Utility\Utility;

    $obj->setData($_POST);

    $obj->store();

    Utility::redirect("index.php");