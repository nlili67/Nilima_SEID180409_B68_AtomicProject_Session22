-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2017 at 08:31 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b68`
--
CREATE DATABASE IF NOT EXISTS `atomic_project_b68` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `atomic_project_b68`;

-- --------------------------------------------------------

--
-- Table structure for table `birthdate`
--

CREATE TABLE `birthdate` (
  `id` int(15) NOT NULL,
  `user_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `is_trashed` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `birthdate`
--

INSERT INTO `birthdate` (`id`, `user_name`, `date`, `is_trashed`) VALUES
(7, 'kamal', '2017-09-25', '2017-09-30 20:25:05'),
(8, 'roni', '2017-09-21', 'NO'),
(10, 'rurma', '2017-09-05', 'NO'),
(11, 'masum', '2017-09-21', 'NO'),
(12, 'minhaj', '2017-09-14', 'NO'),
(13, 'motin', '2017-09-30', 'NO'),
(14, 'minhaj', '2017-09-15', 'NO'),
(15, 'rokib', '2017-09-24', 'NO'),
(16, 'rokib', '2017-09-24', 'NO'),
(17, 'kalam', '2017-09-15', 'NO'),
(18, 'kamal', '2017-09-17', 'NO'),
(19, 'rokib uddin', '2017-09-30', 'NO'),
(20, 'rokib uddin', '2017-09-30', 'NO'),
(23, 'Eimon', '2017-09-22', 'NO'),
(24, 'malisha', '2017-09-30', 'NO'),
(25, 'kalam', '2017-09-23', 'NO'),
(35, 'kalam', '2017-09-30', 'NO'),
(36, 'tanvir', '2017-09-01', 'NO'),
(37, 'ria', '2017-09-17', 'NO'),
(43, 'momme', '2017-09-24', 'NO'),
(44, 'munna', '2017-09-29', 'NO'),
(45, 'koli', '2017-09-25', 'NO'),
(46, 'momme', '2017-09-24', 'NO'),
(47, 'momme', '2017-09-24', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(15) NOT NULL,
  `book_title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_trashed`) VALUES
(4, 'data communication', 'Dr halal', 'NO'),
(5, 'english', 'Dr rosid ahamed', 'NO'),
(8, 'math2', 'kaisar', 'NO'),
(9, 'math2', 'kaisar', 'NO'),
(10, 'g.knowledge', 'rakib', '2017-09-30 14:48:06'),
(11, 'bangla', 'robiul', '2017-09-30 14:48:06'),
(12, 'english', 'Rakib', 'NO'),
(13, 'roman', 'manki', 'NO'),
(14, 'bangla', 'kamal uddin', 'NO'),
(15, 'English', 'abu jahid', 'NO'),
(16, 'Math', 'Kamal uddin', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(15) NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city`, `is_trashed`) VALUES
(5, 'Basail', '2017-09-30 15:15:09'),
(8, 'BAGERHAT', '2017-09-30 15:15:09'),
(9, 'NARSINGDI', '2017-09-30 15:15:10'),
(13, 'BARISAL', '2017-09-30 15:15:21'),
(14, 'FARIDPUR', '2017-09-30 15:15:21'),
(15, 'GAZIPUR', '2017-09-30 15:15:21'),
(17, 'MYMENSINGH', 'NO'),
(18, 'NETRAKONA', 'NO'),
(19, 'SHARIATPUR', 'NO'),
(20, 'NARSINGDI', 'NO'),
(21, 'SHARIATPUR', 'NO'),
(22, 'Elenga', 'NO'),
(23, 'Madhupur', 'NO'),
(24, 'NARSINGDI', 'NO'),
(25, 'CHUADANGA', 'NO'),
(26, 'GOPALGANJ', 'NO'),
(27, 'GOPALGANJ', 'NO'),
(28, 'NARAYANGANJ', 'NO'),
(29, 'SHARIATPUR', 'NO'),
(30, 'SHERPUR', 'NO'),
(31, 'KHULNA', 'NO'),
(32, 'KHULNA', 'NO'),
(33, 'RONGPUR', 'NO'),
(34, 'DHAKA', 'NO'),
(35, 'RONGPUR', 'NO'),
(36, 'DHAKA', 'NO'),
(37, 'RONGPUR', 'NO'),
(38, 'KHULNA', 'NO'),
(39, 'KHULNA', 'NO'),
(40, 'DHAKA', 'NO'),
(41, 'DHAKA', 'NO'),
(42, 'DHAKA', 'NO'),
(43, 'Karatia', 'NO'),
(44, 'Gopalpur', 'NO'),
(45, 'Gopalpur', 'NO'),
(46, 'Madhabdi', 'NO'),
(47, 'Basail', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(15) NOT NULL,
  `customer_Id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `customer_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `customer_Id`, `customer_name`, `email`, `is_trashed`) VALUES
(6, '#0909090', 'Faried uddin ahamed', 'faried@gmail.com', '2017-09-30 14:32:57'),
(8, '#E9190', 'ramim', 'ramim@gmail.com', '2017-09-30 14:32:57'),
(9, '#ddEE', 'rimon borua', 'rimon@gmail.com', '2017-09-30 14:32:57'),
(10, '#gf44', 'roni', 'roni@gmail.com', 'NO'),
(11, '45869', 'komol', 'komol@gmail.com', 'NO'),
(12, '66677', '34322', 'roki0888@gmail.com', 'NO'),
(13, '47uu', 'noman', 'noman@gmail.com', 'NO'),
(14, 'boltu222', 'boltu dass', 'boltu@gmail.com', 'NO'),
(15, 'tanvir1234', 'TANVIR UDDIN', 'tanvir@gmail.com', 'NO'),
(16, '444', 'minhaj', 'minhaj@gmail.com', 'NO'),
(17, '78743', 'kalam khan', 'kalam@gmail.com', 'NO'),
(18, '#898434', 'Moyna', 'moyna@gmail.com', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(15) NOT NULL,
  `Applicant_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `Applicant_name`, `gender`, `is_trashed`) VALUES
(4, 'kamal uddin', 'Array', 'NO'),
(5, 'hasina khatun', 'Female', 'NO'),
(8, 'tanvir', 'Male', 'NO'),
(9, 'jamal', 'Male', 'NO'),
(11, 'rokib', 'Male', 'NO'),
(12, 'rokib', 'Male', 'NO'),
(13, 'manik', 'Male', 'NO'),
(21, 'mamun', 'Male', 'NO'),
(22, 'maisa', 'Female', 'NO'),
(23, 'kalam', 'Male', 'NO'),
(24, 'nahid', 'Male', 'NO'),
(25, 'ruma', 'Female', 'NO'),
(26, 'maimuna', 'Female', 'NO'),
(27, 'niha', 'Female', 'NO'),
(28, 'manusa', 'Female', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(15) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `hobbies` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`, `is_trashed`) VALUES
(18, 'manik', 'Eating, Riding, Photography', 'NO'),
(19, 'malik', 'Eating, Riding, Photography', 'NO'),
(20, 'mank', 'Eating, Photography', 'NO'),
(21, 'kibria', 'Eating, Photography', 'NO'),
(23, 'munna', 'Riding, Photography', 'NO'),
(24, 'kamal', 'Riding, Photography', 'NO'),
(25, 'zaky', 'Eating, Photography', 'NO'),
(26, 'roki', 'Photography', 'NO'),
(27, 'mina', 'Eating', 'NO'),
(29, 'kamal', 'Eating', 'NO'),
(31, 'masum', 'Riding, Photography', 'NO'),
(32, 'minhaj', 'Eating, Riding', 'NO'),
(33, 'masum', 'Eating', 'NO'),
(34, 'rimon', 'Riding', 'NO'),
(35, 'korim', 'Riding, Photography', 'NO'),
(39, 'monsur', 'Eating, Photography', 'NO'),
(40, 'jamal', 'Riding', 'NO'),
(41, 'khirul', 'Eating, Riding, Photography', 'NO'),
(42, 'mijan', 'Eating, Riding', 'NO'),
(43, 'mamun', 'Eating, Riding', 'NO'),
(44, 'jihan', 'Eating', 'NO'),
(45, 'mailsha', 'Eating, Photography', 'NO'),
(46, 'mailsha', 'Eating, Photography', 'NO'),
(47, 'mamun', 'Eating', 'NO'),
(48, 'koli', 'Eating', 'NO'),
(49, 'khaled', 'Eating, Photography', 'NO'),
(50, 'bristi', 'Eating, Riding, Photography', 'NO'),
(51, 'akas', 'Riding', 'NO'),
(52, 'himu', 'Riding', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(15) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `profile_picture` blob NOT NULL,
  `is_trashed` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `profile_picture`, `is_trashed`) VALUES
(44, 'rimon', 0x31353036373538393934323031342d31302d30342031362e31322e31302e6a7067, '2017-09-30 20:22:59'),
(45, 'Rakib Uddin Ahamed', 0x31353036373539303033323031352d30322d32322d31342d32382d35362d3036372e6a7067, '2017-09-30 20:23:00'),
(46, 'minhaj', 0x31353036373539303233323031352d30372d30342d32312d33382d34362d3531362e6a7067, '2017-09-30 20:23:00'),
(47, 'mamun', 0x31353036373539303334323031342d31302d30342031332e30352e31302e6a7067, 'NO'),
(48, 'joya', 0x31353036373539303536323031352d30372d32302d31322d33382d30392d3437322e6a7067, 'NO'),
(49, 'lokman', 0x31353036373539303636323031352d30352d30382d32332d32372d31312d3336372e6a7067, 'NO'),
(50, 'munna', 0x31353036373539303833323031352d30372d32312d31372d33372d35392d3837352e6a7067, 'NO'),
(51, 'momin', 0x31353036373539303937323031352d31302d32332d30342d34302d31322d3437392e6a7067, 'NO'),
(52, 'kalam', 0x3135303637353931313331313230313631315f3934343936343536323234353635335f333739323933343732383030393536313938345f6e2e6a7067, 'NO'),
(53, 'kamal', 0x31353036373539313233323031352d30372d32312d31372d32382d30382d3934302e6a7067, 'NO'),
(54, 'momin', 0x31353036373539313632323031342d31302d30342031362e31322e31302e6a7067, 'NO'),
(55, 'minhaj', 0x3135303637353931383132303135303732355f3037333232312830292e6a7067, 'NO'),
(56, 'minhaj', 0x31353036373539323338323031352d31302d32332d30362d30382d34362d3539362e6a7067, 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `summary`
--

CREATE TABLE `summary` (
  `id` int(15) NOT NULL,
  `Organization_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `summary` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `summary`
--

INSERT INTO `summary` (`id`, `Organization_name`, `summary`, `is_trashed`) VALUES
(11, 'sotula asraful ulum nurani madrash', '                good', 'NO'),
(12, 'arobic academy coaching center', '               good', 'NO'),
(14, 'BITM', '                very good', 'NO'),
(15, 'motobi adarsho high school', '                good', 'NO'),
(16, 'motobi adarsho high school', '                dfgdfgfd', 'NO'),
(17, 'SIBL', 'good', 'NO'),
(18, 'KFC', 'very good', 'NO'),
(19, 'DBL', 'very good', 'NO'),
(20, 'CBL', 'very good', 'NO'),
(21, 'ISBL', 'very good', 'NO'),
(22, 'BTCL', 'Very good', 'NO'),
(23, 'BBL', 'GOOD', 'NO'),
(24, 'DBl', 'Very Good', 'NO'),
(25, 'AACC', 'Very Good', 'NO'),
(26, 'SOME', 'Very Good', 'NO'),
(27, 'OLB', 'good', 'NO'),
(28, 'BDL', 'good', 'NO');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthdate`
--
ALTER TABLE `birthdate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary`
--
ALTER TABLE `summary`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthdate`
--
ALTER TABLE `birthdate`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `summary`
--
ALTER TABLE `summary`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
