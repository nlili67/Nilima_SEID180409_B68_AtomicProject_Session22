<?php
namespace App\Gender;
use App\Message\Message;
use App\Model\Database;
use PDO;


class Gender extends Database
{

    public $id, $applicantName, $Gender;

    public function setData($postArray)
    {

        if (array_key_exists("id", $postArray))
            $this->id = $postArray["id"];

        if (array_key_exists("applicantName", $postArray))
            $this->applicantName = $postArray["applicantName"];

        if (array_key_exists("Gender", $postArray))
            $this->Gender = $postArray["Gender"];


    }// end of setData Method

    public function store()
    {

        $sqlQuery = "INSERT INTO gender (Applicant_name,gender) VALUES ( ? , ?)";

        $sth = $this->dbh->prepare($sqlQuery);

        $dataArray = [$this->applicantName, $this->Gender];

        $status = $sth->execute($dataArray);

        if ($status)

            Message::message("Success! Data has been inserted successfully<br>");
        else
            Message::message("Failed! Data has not been inserted<br>");

    }// end of store() Method

    public function index()
    {

        $sqlQuery = "SELECT * FROM gender  WHERE is_trashed='NO' ORDER BY id DESC ";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();
        return $allData;

    }// end of index() Method

    public function view()
    {

        $sqlQuery = " SELECT * FROM gender WHERE id=" . $this->id;

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $oneData = $sth->fetch();
        return $oneData;

    }// end of view() Method


    public function delete()
    {

        $sqlQuery = "DELETE FROM gender where id=" . $this->id;

        $status = $this->dbh->exec($sqlQuery);


        if ($status)

            Message::message("Success! Data has been deleted successfully<br>");
        else
            Message::message("Failed! Data has not been deleted<br>");


    }//end of delete() Method


    public function update()
    {

        $sqlQuery = "UPDATE gender SET Applicant_name=?, gender = ? WHERE id=" . $this->id;


        $dataArray = [$this->applicantName, $this->Gender];


        $sth = $this->dbh->prepare($sqlQuery);

        $status = $sth->execute($dataArray);

        if ($status)
            Message::setMessage("Success! Data has been updated successfully. <br>");
        else
            Message::setMessage("Failed! Data has not been updated. <br>");


    }


    public function trash()
    {


        $sqlQuery = "UPDATE gender SET is_trashed=NOW() WHERE id=" . $this->id;


        $status = $this->dbh->exec($sqlQuery);

        if ($status)

            Message::message("Success! Data has been trashed successfully<br>");
        else
            Message::message("Failed! Data has not been trashed<br>");


    }// end of trash()

    public function trashed()
    {

        $sqlQuery = "SELECT * FROM gender  WHERE is_trashed<>'NO'";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData = $sth->fetchAll();
        return $allData;

    }// end of trashed() Method

    public function recover()
    {


        $sqlQuery = "UPDATE gender SET is_trashed='NO' WHERE id=" . $this->id;


        $status = $this->dbh->exec($sqlQuery);

        if ($status)

            Message::message("Success! Data has been recovered successfully<br>");
        else
            Message::message("Failed! Data has not been recovered<br>");


    }// end of recover()

    public function indexPaginator($page = 1, $itemsPerPage = 3)
    {


        $start = (($page - 1) * $itemsPerPage);  // start means offset
        if ($start < 0) $start = 0;
        $sql = "SELECT * from gender  WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


        $STH = $this->dbh->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();
        return $arrSomeData;


    }// end of indexPaginator() Method

    public function trashMultiple($selectedIDs)
    {

        if (count($selectedIDs) == 0) {
            Message::message("Empty Selection! Please Select Some Records.<br>");
            return;
        }


        $status = true;

        foreach ($selectedIDs as $id) {

            $sqlQuery = "UPDATE gender SET is_trashed=NOW() WHERE id=$id";

            if (!$this->dbh->exec($sqlQuery)) $status = false;
        }

        if ($status)
            Message::message("Success! All Selected Data Has Been Trashed Successfully<br>");
        else
            Message::message("Failed! All Selected Data Has Not Been Trashed<br>");


    }// end of trashMultiple

    public function deleteMultiple($selectedIDs)
    {

        if (count($selectedIDs) == 0) {
            Message::message("Empty Selection! Please Select Some Records.<br>");
            return;
        }


        $status = true;

        foreach ($selectedIDs as $id) {

            $sqlQuery = "DELETE FROM gender WHERE id=$id";

            if (!$this->dbh->exec($sqlQuery)) $status = false;
        }

        if ($status)
            Message::message("Success! All Selected Data Has Been Deleted Successfully<br>");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted<br>");


    }// end of deleteMultiple


    public function recoverMultiple($selectedIDs)
    {

        if (count($selectedIDs) == 0) {
            Message::message("Empty Selection! Please Select Some Records.<br>");
            return;
        }


        $status = true;

        foreach ($selectedIDs as $id) {

            $sqlQuery = "UPDATE gender SET is_trashed='NO' WHERE id=$id";

            if (!$this->dbh->exec($sqlQuery)) $status = false;
        }

        if ($status)
            Message::message("Success! All Selected Data Has Been Recovered Successfully<br>");
        else
            Message::message("Failed! All Selected Data Has Not Been Recovered<br>");


    }// end of recoverMultiple


    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->Applicant_name);
        }


        foreach ($allData as $oneData) {

            $eachString = strip_tags($oneData->Applicant_name);
            $eachString = trim($eachString);
            $eachString = preg_replace("/\r|\n/", " ", $eachString);
            $eachString = str_replace("&nbsp;", "", $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord) {
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->gender);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString = strip_tags($oneData->gender);
            $eachString = trim($eachString);
            $eachString = preg_replace("/\r|\n/", " ", $eachString);
            $eachString = str_replace("&nbsp;", "", $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord) {
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords

    public function search($requestArray)
    {
        $sql = "";
        if (isset($requestArray['byApplicant']) && isset($requestArray['byGender']))
            $sql = "SELECT * FROM `gender` WHERE `is_trashed` ='No' AND (`Applicant_name` LIKE '%" . $requestArray['search'] . "%' OR `gender` LIKE '%" . $requestArray['search'] . "%')";
        if (isset($requestArray['byApplicant']) && !isset($requestArray['byGender']))
            $sql = "SELECT * FROM `gender` WHERE `is_trashed` ='No' AND `Applicant_name` LIKE '%" . $requestArray['search'] . "%'";
        if (!isset($requestArray['byApplicant']) && isset($requestArray['byGender']))
            $sql = "SELECT * FROM `gender` WHERE `is_trashed` ='No' AND `gender` LIKE '%" . $requestArray['search'] . "%'";

        $STH  = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()



}

