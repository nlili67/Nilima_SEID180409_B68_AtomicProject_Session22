<?php
namespace App\City;
use App\Message\Message;
use App\Model\Database;
use PDO;


class City extends Database
{
public $id,$City;

    public function setData($postArray)
    {

        if (array_key_exists("id", $postArray))
            $this->id = $postArray["id"];


        if (array_key_exists("City", $postArray))
            $this->City = $postArray["City"];

    }// end of setData Method

    public function store()
    {

        $sqlQuery = "INSERT INTO City (City) VALUES (?)";
        $sth = $this->dbh->prepare($sqlQuery);


        $dataArray = [$this->City];

        $status = $sth->execute($dataArray);

        if($status)

            Message::message("Success! Data has been inserted successfully<br>");
        else
            Message::message("Failed! Data has not been inserted<br>");

    }// end of store() Method

    public function index(){

        $sqlQuery = "SELECT * FROM city  WHERE is_trashed='NO'";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData=  $sth->fetchAll();
        return $allData;

    }// end of index() Method

    public function trash(){


        $sqlQuery = "UPDATE city SET is_trashed=NOW() WHERE id=".$this->id;


        $status = $this->dbh->exec($sqlQuery);

        if($status)

            Message::message("Success! Data has been trashed successfully<br>");
        else
            Message::message("Failed! Data has not been trashed<br>");

    }// end of trash()

    public function trashed(){

        $sqlQuery = "SELECT * FROM city  WHERE is_trashed<>'NO'";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData=  $sth->fetchAll();
        return $allData;

    }// end of trashed() Method

    public function recover(){


        $sqlQuery = "UPDATE city SET is_trashed='NO' WHERE id=".$this->id;


        $status = $this->dbh->exec($sqlQuery);

        if($status)

            Message::message("Success! Data has been recovered successfully<br>");
        else
            Message::message("Failed! Data has not been recovered<br>");



    }// end of recover()


    public function view(){

        $sqlQuery = " SELECT * FROM City WHERE id= ".$this->id;
        $sth = $this->dbh->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $oneData=$sth->fetch();
        return $oneData;

    }// end of view() Method

    public function delete(){

        $sqlQuery = "DELETE FROM City where id=".$this->id;

        $status = $this->dbh->exec($sqlQuery);


        if($status)

            Message::message("Success! Data has been deleted successfully<br>");
        else
            Message::message("Failed! Data has not been deleted<br>");


    }//end of delete() Method

    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);  // start means offset
        if($start<0) $start = 0;
        $sql = "SELECT * from city  WHERE is_trashed = 'No' LIMIT $start,$itemsPerPage";


        $STH = $this->dbh->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }// end of indexPaginator() Method

    public function trashMultiple($selectedIDs){

        if( count($selectedIDs) == 0 ){
            Message::message("Empty Selection! Please Select Some Records.<br>");
            return;
        }


        $status = true;

        foreach ($selectedIDs as $id){

            $sqlQuery = "UPDATE city SET is_trashed=NOW() WHERE id=$id";

            if( !  $this->dbh->exec($sqlQuery)  )   $status = false;
        }

        if($status)
            Message::message("Success! All Selected Data Has Been Trashed Successfully<br>");
        else
            Message::message("Failed! All Selected Data Has Not Been Trashed<br>");



    }// end of trashMultiple

    public function deleteMultiple($selectedIDs){

        if( count($selectedIDs) == 0 ){
            Message::message("Empty Selection! Please Select Some Records.<br>");
            return;
        }


        $status = true;

        foreach ($selectedIDs as $id){

            $sqlQuery = "DELETE FROM city  WHERE id=$id";

            if( !  $this->dbh->exec($sqlQuery)  )   $status = false;
        }

        if($status)
            Message::message("Success! All Selected Data Has Been Deleted Successfully<br>");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted<br>");



    }// end of deleteMultiple


    public function recoverMultiple($selectedIDs){

        if( count($selectedIDs) == 0 ){
            Message::message("Empty Selection! Please Select Some Records.<br>");
            return;
        }


        $status = true;

        foreach ($selectedIDs as $id){

            $sqlQuery = "UPDATE city SET is_trashed='NO' WHERE id=$id";

            if( !  $this->dbh->exec($sqlQuery)  )   $status = false;
        }

        if($status)
            Message::message("Success! All Selected Data Has Been Recovered Successfully<br>");
        else
            Message::message("Failed! All Selected Data Has Not Been Recovered<br>");



    }// end of recoverMultiple


    public function update(){

        $sqlQuery = "UPDATE city SET city=? WHERE id=".$this->id;

        $sth = $this->dbh->prepare( $sqlQuery );


        $dataArray = [ $this->City ];

        $status = $sth->execute($dataArray);

        if($status)

            Message::message("Success! Data has been updated successfully<br>");
        else
            Message::message("Failed! Data has not been updated<br>");

    }// end of store() Method


    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->id);
        }



        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->id);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->city);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->city);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords


    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byId']) && isset($requestArray['byCity']) )
            $sql = "SELECT * FROM `city` WHERE `is_trashed` ='No' AND (`city` LIKE '%".$requestArray['search']."%' OR `id` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byId']) && !isset($requestArray['byCity']) )
            $sql = "SELECT * FROM `city` WHERE `is_trashed` ='No' AND `id` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byId']) && isset($requestArray['byCity']) )
            $sql = "SELECT * FROM `city` WHERE `is_trashed` ='No' AND `city` LIKE '%".$requestArray['search']."%'";




        $STH  = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()








}